package com.sigmotoa.codes.workshop;

import java.util.Arrays;

/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */
public class Agility {

    //bigger than

    //Show if the first number is bigger than the second
    public static boolean biggerThan(String numA, String numB)
    {
        boolean rta;
        double nA,nB;

        rta = false;

        nA= Double.parseDouble(numA);
        nB= Double.parseDouble(numB);

        if (nA > nB)
        {
            rta =true;
        }
        return rta;

    }

    //Sort from bigger the numbers an show in array
    public static int[] order(int numA, int numB, int numC,int numD,int numE)
    {
        int [] n= new int []{numA,numB,numC,numD,numE};
        Arrays.sort(n);

        return n;
    }

    //Look for the smaller number of the array
    public static double smallerThan(double array [])
    {
        double m;
        m = array[0];

        for(int d = 0; d < array.length; d++)
        {
            if(array[d] < m)
            {
                m = array[d];
            }
        }

        return m;
    }

    //Palindrome number is called in Spanish capicúa
    //The number is palindrome
    public static boolean palindromeNumber(Integer numA)
    {
        int rna;
        boolean rta;

        rta = false;

        rna = Integer.reverse(numA);
        if(rna == numA)
        {
            rta = true;
        }
        return rta;
        //unsolved code
    }

    //the word is palindrome
    public static boolean palindromeWord(String word)
    {
        //unsolved code
        return false;
    }

    //Show the factorial number for the parameter
    public static int factorial(int numA)
    {
        int fact = 1;

        if(numA == 0)
        {
            fact = 1;
        }
        else
        {
            for(int p = 1; p <= numA; p++)
            {
                fact = fact*p;
            }
        }
        return fact;
        //solved code
    }

    //is the number odd
    public static boolean isOdd(byte numA)
    {
        boolean rta;
        rta = true;

        if ((numA % 2) == 0)
        {
            rta = false;
        }

        return rta;
        //solved code
    }

    //is the number prime
    public static boolean isPrimeNumber(int numA)
    {
        boolean rta;
        int m_num;

        m_num = 0;
        rta = false;

        //this code validates how many divisors does the number have
        for (int x = 1; x <= numA; x++)
        {
            if((numA % x) == 0)
            {
                m_num = m_num + 1;
            }
        }

        //if the number have 2 or less divisors means the number is prime
        if (m_num == 2)
        {
            rta = true;
        }
        return rta;
        //solved code
    }

    //is the number even
    public static boolean isEven(byte numA)
    {
        boolean rta;

        rta = false;

        if((numA % 2) == 0)
        {
            rta = true;
        }

        return rta;
        //solved code
    }

    //is the number perfect
    public static boolean isPerfectNumber(int numA)
    {
        boolean rta;
        int suma;

        suma = 0;

        rta = false;

        for(int o =1; o < numA; o++)
        {
            //This section defines the divisors of the number and sum them
            if((numA % o) == 0)
            {
                suma = suma + o;
            }

            if(suma == numA)
            {
                rta = true;
            }
        }

        return rta;
        //solved code
    }

    //Return an array with the fibonacci sequence for the requested number
    public static int [] fibonacci(int numA)
    {
        int[] secuencia;

        secuencia = new int [numA+1];

        secuencia[0] = 0;
        secuencia[1] = 1;
        if(numA >= 2)
        {
            for (int x = 2; x < secuencia.length;x++)
            {
                secuencia[x] = secuencia[x - 1] + secuencia[x - 2];
            }
        }
        return secuencia;
        //solved code
    }

    //how many times the number is divided by 3
    public static int timesDividedByThree(int numA)
    {
        int tiempo;

        tiempo = 0;

        for(int t = 1; t <= numA; t++)
        {
            if((t % 3) == 0)
            {
                tiempo = tiempo +  1;
            }
        }
        return tiempo;
    }

    //The game of fizzbuzz
    public static String fizzBuzz(int numA)
    /**
     * If number is divided by 3, show fizz
     * If number is divided by 5, show buzz
     * If number is divided by 3 and 5, show fizzbuzz
     * in other cases, show the number
     */
    {
        String palabra;

        palabra = Integer.toString(numA);

        if( ((numA % 3) == 0) && ((numA % 5) == 0) )
        {
            palabra= "FizzBuzz";
        }
        else if((numA % 3) == 0)
        {
            palabra= "Fizz";
        }
        else if((numA % 5) == 0)
        {
            palabra = "Buzz";
        }

        return palabra;
        //solved code
    }
}
