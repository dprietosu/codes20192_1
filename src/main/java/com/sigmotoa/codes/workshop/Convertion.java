package com.sigmotoa.codes.workshop;

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
public class Convertion {

    //convert of units of Metric System

    //Km to metters
    public static int kmToM1(double km)
    {
        //K means the value of one Km into M
        double k;
        int M;

        k=1000;

        M = (int) Math.round(km*k);

        return M;
    }

    //Km to metters
    public static double kmTom(double km)
    {
        //K means the value of one Km into M
        double M,K;
        M = 0;
        K = 1000;
        M = km*K;
        return M;
    }

    //Km to cm
    public static double kmTocm(double km)
    {
        //K means the value of one Km into Cm
        double Cm,K;
        Cm = 0;
        K = 100000;
        Cm = km*K;

        return Cm;
    }

    //milimetters to metters
    public static double mmTom(int mm)
    {
        //K means the value of one milimeter into metters
        double m1,K;
        m1 = 0;
        K = 0.001;
        m1 = mm*K;

        return m1;
    }
//convert of units of U.S Standard System

    //convert miles to foot
    public static double milesToFoot(double miles)
    {
        //K means the value of one mile into foot
        double fo,K;
        fo = 0;
        K = 5280;
        fo = miles*K;

        return fo;
    }

    //convert yards to inches
    public static int yardToInch(int yard)
    {
        //K means the value of one yard into inch
        int i1,K;
        i1 = 0;
        K = 36;
        i1 = yard*K;

        return i1;
    }

    //convert inches to miles
    public static double inchToMiles(double inch)
    {
        //K means the value of one inch into miles
        double m,K;
        m = 0;
        K = 0.000016;
        m = inch*K;

        return m;
    }
    //convert foot to yards
    public static int footToYard(int foot)
    {
        //K means the value of one foot into yard
        double K;
        int y;
        y = 0;
        K = 0.333333;
        y = (int) Math.floor(foot*K);

        return y;
    }

//Convert units in both systems

    //convert Km to inches
    public static double kmToInch(String km)
    {
        //K means the value of one Km into Inch
        double m,K;
        m = 0;
        K = 39370.0787;

        m = Double.parseDouble(km)*K;

        return m;
    }

    //convert milimmeters to foots
    public static double mmToFoot(String mm)
    {
        //K means the value of one mm into Foot
        double m,K;

        m = 0;
        K = 0.003281;

        m = Double.parseDouble(mm)*K;

        return m;
    }
    //convert yards to cm
    public static double yardToCm(String yard)
    {
        //K means the value of one Yard into cm
        double m,K;

        m = 0;
        K = 91.44;


        m = Double.parseDouble(yard) * K;

        return m;
    }
}
