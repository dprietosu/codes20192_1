package com.sigmotoa.codes.workshop;
/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Geometric Exercises
 */


import static java.lang.Math.*;

public class Geometric {

    //Calculate the area for a square using one side
    public static int squareArea(int side)
    {
        int rta;

        rta= 0;

        if(side > 0)
        {
            rta = side * side;
        }
        else if(side <= 0)
        {
            throw new IllegalArgumentException();
        }

        return rta;
        //solved code
    }

    //Calculate the area for a square using two sides
    public static int squareArea(int sidea, int sideb)
    {
        int rta ;
        if(sidea > 0 && sideb > 0)
        {
            rta = sidea * sideb;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return rta;
        //solved code
    }

    //Calculate the area of circle with the radius
    public static double circleArea(double radius)
    {
        double rta;
        if(radius >= 0 )
        {
            rta = (radius*radius)*3.1416;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return rta;
    }

    //Calculate the perimeter of circle with the diameter
    public static double circlePerimeter(int diameter)
    {
        double rta;
        if(diameter >= 0 )
        {
            rta = diameter*3.1416;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return rta;
    }

    //Calculate the perimeter of square with a side
    public static double squarePerimeter(double side)
    {
        double rta;
        if(side >= 0 )
        {
            rta = 4*side;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return rta;
    }

    //Calculate the volume of the sphere with the radius
    public static double sphereVolume(double radius)
    {
        double rta;
        if(radius >= 0 )
        {
            double r3;
            float div;

            //define div to set a precise value of 3/4
            r3= radius*radius*radius;
            div = (float) 4/3;

            rta = (div)*(3.1416*r3);
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return rta;
    }

    //Calculate the area of regular pentagon with one side
    public static float pentagonArea(int side)
    {
        float rta,lado,Area;
        double a,angulo,r_angulo;

        angulo= 36;
        Area = 0;

        //this section finds the high of the pentagon
        r_angulo= Math.toRadians(angulo);
        lado= (float) side/2;
        a= (lado)/Math.tan(r_angulo);

        if(side >= 0 )
        {
            rta =  (float) ( ( (lado*a)/2 )*10 );
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return rta;
    }

    //Calculate the Hypotenuse with two cathetus
    public static double calculateHypotenuse(double catA, double catB)
    {
        double rta;
        double catA1,catB1;
        if(catA >= 0 && catB >= 0)
        {
            catA1 = catA*catA;
            catB1 = catB* catB;

            rta = Math.sqrt(catA1+catB1);
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return rta;
    }

}
